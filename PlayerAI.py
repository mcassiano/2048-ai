#!/usr/bin/env python
#coding:utf-8

from random import randint
from BaseAI import BaseAI
from ComputerAI import ComputerAI
import math

class PlayerAI(BaseAI):


	weight_matrices = [
				[
					[3, 2, 1,  0],
					[2, 1, 0,  -1],
					[1, 0, -1,  -2],
					[0, -1, -2,  -3]
				],

				[
					[0, 1, 2, 3],
					[-1, 0, 1, 2],
					[-2, -1, 0, 1],
					[-3, -2, -1, 0]
				],
	]

	def __init__(self):
		from GameManager import defaultPossibility
		self.defaultPossibility = defaultPossibility


	def getMove(self, grid):

		result = self.find_best_move(grid, 5)

		if result['direction'] == None:
			moves = grid.getAvailableMoves()
			return moves[randint(0, len(moves) - 1)]

		return result['direction']

	def find_best_move(self, grid, depth):

		alpha = {
			'score': float("-inf"),
			'direction': None
		}

		beta = {
			'score': float("inf"),
			'direction': None
		}

		result = self.alphabeta(grid, depth, alpha, beta, True)
		return result


	def alphabeta(self, grid, depth, alpha, beta, maximize, best_move = -1):

		if not grid.canMove():
			return {'score': 0, 'direction': best_move}

		if depth == 0:
			return {'score': self.heuristic(grid), 'direction': best_move}


		if maximize:
			moves = grid.getAvailableMoves()
			for move in moves:
				grid_copy = grid.clone()
				grid_copy.move(move)

				mv = move if best_move == -1 else best_move
				alpha = max([alpha, self.alphabeta(grid_copy, depth - 1, alpha, beta, False, mv)], key=lambda item: item['score'])
				if beta['score'] <= alpha['score']:
					break

			return alpha

		else:
			moves = grid.getAvailableCells()
			values = [2,4]

			if not moves:
				return self.alphabeta(grid, depth - 1, alpha, beta, True, best_move)

			for cell in moves:
				
				if randint(0,99) < 100 * self.defaultPossibility: 
					value = 2
				else: 
					value = 4

				grid_copy = grid.clone()
				grid_copy.setCellValue(cell, value)

				beta = min([beta, self.alphabeta(grid_copy, depth - 1, alpha, beta, True, best_move)], key=lambda item: item['score'])
				if beta['score'] <= alpha['score']:
					break

			return beta


	def heuristic(self, grid):

		best = 0
		grid_array = grid.map

		for i in range(2):
			s = 0
			for y in range(4):
				for x in range(4):
					s += self.weight_matrices[i][y][x]*grid_array[x][y]
			s = abs(s)

			if (s > best):
				best = s

		return best
