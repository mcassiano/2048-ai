#!/usr/bin/env python
#coding:utf-8

from random import randint
from BaseAI import BaseAI
import math

class ComputerAI(BaseAI):

	weight_matrices = [
				[
					[3, 2, 1,  0],
					[2, 1, 0,  -1],
					[1, 0, -1,  -2],
					[0, -1, -2,  -3]
				],

				[
					[0, 1, 2, 3],
					[-1, 0, 1, 2],
					[-2, -1, 0, 1],
					[-3, -2, -1, 0]
				],
	]

	def __init__(self):
		from GameManager import defaultPossibility
		self.defaultPossibility = defaultPossibility


	def getMove(self, grid):
		result = self.find_best_move(grid, 5)		

		if result['cell'] == None:
			moves = grid.getAvailableCells()
			return moves[randint(0, len(moves) - 1)]

		return result['cell']

	def find_best_move(self, grid, depth):

		alpha = {
			'score': float('-inf'),
			'cell': None
		}

		beta = {
			'score': float('inf'),
			'cell': None
		}

		result = self.alphabeta(grid, depth, alpha, beta, False)
		return result

	def alphabeta(self, grid, depth, alpha, beta, maximize, best_cell = -1):

		if not grid.canMove():
			result = {
				'score': float("inf"),
				'cell': best_cell
			}
			return result

		if depth == 0:
			result = {
				'score': self.heuristic(grid),
				'cell': best_cell
			}
			return result


		if maximize:
			moves = grid.getAvailableMoves()
			for move in moves:
				grid_copy = grid.clone()
				grid_copy.move(move)

				
				alpha = max([alpha, self.alphabeta(grid_copy, depth - 1, alpha, beta, False, best_cell)], key=lambda item: item['score'])
				if beta['score'] <= alpha['score']:
					break

			return alpha

		else:
			moves = grid.getAvailableCells()
			values = [2,4]

			if not moves:
				return self.alphabeta(grid, depth - 1, alpha, beta, True, best_cell)

			for cell in moves:
				
				if randint(0,99) < 100 * self.defaultPossibility: 
					value = 2
				else: 
					value = 4

				grid_copy = grid.clone()
				grid_copy.setCellValue(cell, value)

				mv = cell if best_cell == -1 else best_cell
				beta = min([beta, self.alphabeta(grid_copy, depth - 1, alpha, beta, True, mv)], key=lambda item: item['score'])
				if beta['score'] <= alpha['score']:
					break

			return beta



	def heuristic(self, grid):

		best = 0
		grid_array = grid.map

		for i in range(2):
			s = 0
			for y in range(4):
				for x in range(4):
					s += self.weight_matrices[i][y][x]*grid_array[x][y]
			s = abs(s)

			if (s > best):
				best = s

		return best