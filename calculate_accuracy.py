import GameManager
import Displayer

max_tiles = []
grids = []

for i in range(10):
	grid = GameManager.main()
	max_tiles.append(grid.getMaxTile())
	grids.append(grid)

print '---------------------------------------------'
print 'RESULTS'
for grid in grids:
	print '\n'
	Displayer.Displayer().unixDisplay(grid)
	print '\n\n'

print max_tiles

first_100_run = [32, 16, 32, 32, 64, 32, 1024, 16, 2048, 8, 32, 16, 64, 32, 256, 16, 2048, 64, 1024, 16, 8, 512, 512, 16, 1024, 2048, 1024, 2048, 32, 16, 8, 512, 1024, 2048, 1024, 2048, 512, 8, 16, 2048, 16, 512, 1024, 32, 16, 32, 2048, 8, 512, 2048, 2048, 16, 16, 1024, 512, 16, 2048, 32, 1024, 2048, 2048, 16, 8, 64, 16, 2048, 2048, 2048, 16, 64, 1024, 16, 32, 1024, 512, 512, 2048, 8, 2048, 32, 512, 16, 1024, 2048, 128, 16, 32, 512, 32, 1024, 2048, 2048, 1024, 32, 8, 2048, 16, 1024, 2048, 2048]
